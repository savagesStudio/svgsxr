# svgsXR
A collection of HDAs and Notch blocks/dfx's intended for use in xR/Broadcast/Real-time

If you can read this, we've invited you to check out some of our internal studio tools. Support is not implied by the invite - if you're going to use these in production we'd ask you to get in touch with us at ted@savag.es to work out a support agreement. There's a merge of Old Style and New Style here - Old Style normalizes non-overlapping UVs, New Style makes a main UV set in real-world scale and a second UV set for lights. Which is your preference? 

Most of these do what they say on the tin - here's a quick summary:

svgsArcCarpet - makes a programmable arc with a certain radius. We use this to make radial surfaces for scattering  instances.

svgsArcWall - makes a programmable arc with a wall, and the option to cut arches out of the wall.

svgsArchWall - makes an arch

svgsBasicScatter - scatters objects to points

svgsFont - the Houdini font node, wrapped up for use elsewhere. It's an excellent font tool - for design purposes we make an initial style, copy it to make a secondary style and parent it to the initial object to maintain alignment, and then parent a third copy to the first copy to make a third style if needed. Coming soon - fit text to bounding box, and multiple lines. Right now for multiple lines you need to create and set a single object per-line.

svgsLandSculpture - takes a heightfield in using svgsTerrainLoader, applies it to one of several geometries. Please note that bottom UVs are orthographic from the side, and in proportion where horizontal is from 0-1 and vertical is as tall as it would be in scale. Top UVs are the same as the ones that apply the heightfield, with a dramatic face shift texture stretching will happen. You can cut a tube out of the sculpture as well.

svgsNoiseyDisplacer - uses MOPs (http://motionoperators.com) to drive noise displacement on either an input mesh, or a dense grid we provide. We're still dialing this one in, and would appreciate feedback.

svgsQuickLand pack - svgsTerrainLoader, svgsQuickLandMask, svgsQuickLandScatter - these tools work together to provide a bare-bones, very basic matte painting/terrain previz workflow. svgsTerrainLoader wants a 32bit EXR - if you get blocks in your heightfield, it's not on the asset. svgsQuickLandScatter is intended for Unreal only - the Unreal integration will see instance geometry inputs where primitives initially draw, but other DCCs may not.

svgsRampedWall - applies a ramp, and optionally noise, to a curve in order to create either a wall or ribbon-like element.

svgsScatterWall - fill the camera with a lovely wall of planes.

svgsSingleBezier and svgsBezier16 - a single chunk of bezier curve, and 16 single chunks tied together.

svgsSmokeSolver - for Houdini olny right now - might work in DCCs, will not do anything for you in UE4 or Unity. Wraps the new H18 Smoke SOPs workflow into an HDA. Should've been a shelf tool, to be honest.

svgsSteps - makes stairs with stair tops, no railings in the free version ;)

svgsUVDetiler - addresses tiling by extruding your mesh just a wee, adding a new material set on non-overlapping unwrapped UVs, and merging this mesh back on top of your original geo. Use a material with an alpha channel to see the original mesh. Caution - draws UVs twice, not for use on dense mesh in realtime engines.
